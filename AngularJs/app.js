var appOlegKosarev = angular.module('appOlegKosarev', ['pascalprecht.translate', 'ngCookies', 'slickCarousel'])
        .config(['$translateProvider', '$compileProvider', 'slickCarouselConfig', function ($translateProvider, $compileProvider, slickCarouselConfig) {
                $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension|tel):/);
                // Angular before v1.2 uses $compileProvider.urlSanitizationWhitelist(...)
                $translateProvider.useStaticFilesLoader({
                    prefix: 'i18n/',
                    suffix: '.json'
                });
                $translateProvider.preferredLanguage('ru');
                $translateProvider.useCookieStorage();
                slickCarouselConfig.dots = false;
                slickCarouselConfig.autoplay = false;
            }]);
appOlegKosarev.filter("trust", ['$sce', function ($sce) {
        return function (htmlCode) {
            return $sce.trustAsHtml(htmlCode);
        };
    }]);
appOlegKosarev.controller('ctrl', function ($scope, $translate) {
    $scope.setLang = function (langKey) {
        $translate.use(langKey);
    };
    $scope.yearFooter = "2014 - " + new Date().getFullYear();
});

appOlegKosarev.controller('home', function ($scope) {
    //TODO ADD GET FOR HOME PAGE
});

appOlegKosarev.controller('about', function ($scope, $http) {
    $http({
        method: 'GET',
        url: 'data/about.json'
    }).then(function successCallback(response) {
        var response = response.data;
        $scope.TOOLS_I_USE = response.TOOLS_I_USE;
        $scope.SERVICES_ACORDION = response.SERVICES.ACORDION;
        $scope.IMAGES = response.IMAGES;
        $scope.TESTIMONIALS = response.TESTIMONIALS;
        $scope.DESCRIPTION = response.DESCRIPTION;
        $scope.PERSONALS_DETALIS = response.WHO_I_AM;
        $scope.WHAT_I_DO_TEXT = response.WHAT_I_DO;
        $scope.MY_AWARDS = response.MY_AWARDS;
        $scope.slickCurrentIndex = 0;
        $scope.slickConfigTestimonialsLoaded = true;
        $scope.slickConfigImagesLoaded = true;
        $scope.slickConfigImages = {
            dots: false,
            autoplay: true,
            initialSlide: 3,
            infinite: true,
            autoplaySpeed: 4000,
            prevArrow: '',
            nextArrow: '',
            method: {},
            event: {}
        };
        $scope.slickConfigTestimonials = {
            dots: false,
            autoplay: true,
            initialSlide: 3,
            infinite: true,
            autoplaySpeed: 4500,
            prevArrow: '',
            nextArrow: '',
            method: {},
            event: {}
        };
    }, function errorCallback(response) {
        alert("errorCallback in about controller line resnose text in console");
        console.log(response);
    });
});
appOlegKosarev.controller('resume', function ($scope, $http) {
    $http({
        method: 'GET',
        url: 'data/resume.json'
    }).then(function successCallback(response) {
        var response = response.data;
        $scope.EDUCATION = response.EDUCATION;
        $scope.SKILLS = response.SKILLS;
        $scope.EMPLOYMENT = response.EMPLOYMENT;
        console.log($scope.EMPLOYMENT);
    }, function errorCallback(response) {
        alert("errorCallback in resume controller line resnose text in console");
        console.log(response);
    });
});

